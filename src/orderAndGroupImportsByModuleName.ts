import * as ts from 'typescript';
import * as Lint from 'tslint';
import * as tsutils from 'tsutils';

export class Rule extends Lint.Rules.AbstractRule {
    public static metadata: Lint.IRuleMetadata = {
        ruleName: 'order-and-group-imports-by-module-name',
        description: 'Order and group imports based on regexes.',
        rationale: 'Helps maintain a readable style in your codebase.',
        optionsDescription: Lint.Utils.dedent`
            Options is an array of regexes defining each import group.
        `,
        options: {
            type: 'object',
            properties: {
                "order": {
                    type: "array"
                }
            },
        },
        optionExamples: [[true], [true, { "order": ["/[^a-zA-Z]*/", "/^@.*/", "/^\..*/", ".css$"] }]],
        type: 'typescript',
        typescriptOnly: false,
        hasFix: true
    };

    public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
        return this.applyWithWalker(new OriginOrderedImportWalker(sourceFile, this.ruleName, {}));
    }
}

function expressionIsStringLiteral(expression: ts.Expression): expression is ts.StringLiteral {
    return expression.kind === ts.SyntaxKind.StringLiteral;
}

function nodeIsImportNode(node?: ts.Node): node is AnyImportDeclaration {
    return node && (node.kind === ts.SyntaxKind.ImportDeclaration || node.kind === ts.SyntaxKind.ImportEqualsDeclaration);
}

type AnyImportDeclaration = ts.ImportDeclaration | ts.ImportEqualsDeclaration;

const importOrderRulesRaw = ["^[a-zA-Z]", "^@.*(?!\.css)$", "^\..*(?!\.css)$", "\.css$"];
const importOrderRules = importOrderRulesRaw.map((rule) => new RegExp(rule));

class OriginOrderedImportWalker extends Lint.AbstractWalker<{}> {
    protected lastImportType: number = 0;
    protected fixedOrder: { [key: string]: ts.Node[] } = {};
    protected hasErrors = false;

    public walk(sourceFile: ts.SourceFile) {
        let firstImportLine = null;
        let lastImportLine = null;

        const cb = (node: ts.Node): void => {
            if (nodeIsImportNode(node)) {
                this.check(node);
                this.addSortedImportNode(node);
                if (firstImportLine === null) {
                    firstImportLine = node.getStart();
                }
                if (lastImportLine === null || lastImportLine < node.getEnd()) {
                    lastImportLine = node.getEnd();
                }
            }
            ts.forEachChild(node, cb);
        };
        ts.forEachChild(sourceFile, cb);
        if (this.hasErrors) {
            this.write(firstImportLine, lastImportLine);
        }
    }

    protected write(from: number, to: number) {
        const imports = Object.keys(this.fixedOrder).map((key) => this.fixedOrder[key]);
        const importGroups = imports.map((importGroup) => {
            return importGroup
                .map((node) => node.getText())
                .join(this.getEolChar())
        });
        const nextImports = importGroups.join(this.getEolChar() + this.getEolChar());
        this.addFailureAt(from, to, "Import order must match order and each group must be separated by a blank line", Lint.Replacement.replaceFromTo(from, to, nextImports));
    }

    protected addSortedImportNode(node: AnyImportDeclaration) {
        const importType = this.getImportType(this.getModuleName(node));
        const importTypeNodes = this.fixedOrder[importType] || [];
        importTypeNodes.push(node);
        this.fixedOrder[importType] = importTypeNodes;
    }

    protected getModuleName(node: AnyImportDeclaration): string {
        if (node.kind === ts.SyntaxKind.ImportDeclaration) {
            return this.removeQuotes(node.moduleSpecifier.getText());
        } else if (node.moduleReference.kind === ts.SyntaxKind.ExternalModuleReference) {
            if (expressionIsStringLiteral(node.moduleReference.expression)) {
                return this.removeQuotes(node.moduleReference.expression.text);
            }
        }

        return this.removeQuotes(node.moduleReference.getText());
    }

    protected check(node: AnyImportDeclaration): void {
        const moduleName = this.getModuleName(node);
        const importType = this.getImportType(moduleName);

        this.checkOrder(importType);
        this.checkEmptyLine(node, importType);
    }

    protected checkOrder(importType: number): void {
        if (this.lastImportType > importType) {
            this.hasErrors = true;
        } else {
            this.lastImportType = importType;
        }
    }

    protected checkEmptyLine(node: AnyImportDeclaration, importType: number): void {
        const nextNode = tsutils.getNextStatement(node);

        if (!nodeIsImportNode(nextNode)) {
            return;
        }

        const nextImportType = this.getImportType(this.getModuleName(nextNode));

        if (importType === nextImportType) {
            if (this.blankLinesBetweenNodes(node, nextNode) !== 0) {
                this.hasErrors = true;
            }
            return;
        }

        const blankLinesCount = this.blankLinesBetweenNodes(node, nextNode);
        if (blankLinesCount !== 1) {
            this.hasErrors = true;
        }
    }

    protected getNodeLeadingCommentedLinesCount(node: AnyImportDeclaration): number {
        const comments = ts.getLeadingCommentRanges(this.getSourceFile().text, node.pos);

        if (!comments) return 0;

        return comments
            .reduce((count, comment) => {
                const startLine = ts.getLineAndCharacterOfPosition(this.getSourceFile(), comment.pos).line;
                const endLine = ts.getLineAndCharacterOfPosition(this.getSourceFile(), comment.end).line;

                return count + (endLine - startLine + 1);
            }, 0);
    }

    protected getLine(node: ts.Node) {
        return ts.getLineAndCharacterOfPosition(
            this.getSourceFile(),
            node.getEnd()
        ).line;
    }

    protected getImportType(source: string): number {
        return importOrderRules.findIndex((rule) => rule.test(source));
    }

    protected removeQuotes(value: string): string {
        if (value && value.length > 1 && (value[0] === `'` || value[0] === `"`)) {
            value = value.substr(1, value.length - 2);
        }

        return value;
    }

    protected blankLinesBetweenNodes(nodeA: AnyImportDeclaration, nodeB: AnyImportDeclaration) {
        const nodeLine = this.getLine(nodeA);
        const nextLine = this.getLine(nodeB);
        const linesBetweenNodes = nextLine - nodeLine - 1;
        return linesBetweenNodes - this.getNodeLeadingCommentedLinesCount(nodeB);
    }

    private getEolChar(): string {
        const lineEnd = this.sourceFile.getLineEndOfPosition(0);
        let newLine;
        if (lineEnd > 0) {
            if (lineEnd > 1 && this.sourceFile.text[lineEnd - 1] === "\r") {
                newLine = "\r\n";
            } else if (this.sourceFile.text[lineEnd] === "\n") {
                newLine = "\n";
            }
        }
        return newLine === undefined ? ts.sys.newLine : newLine;
    }
}
